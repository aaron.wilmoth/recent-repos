This will clone all github and gitlab repos and output a list of those that have been updated since the beginning of 2019. 

It requires the pygithub3 module, which you can install on macos (Mac OSX) like this::
    # get pip if you don't have it
    sudo python -m ensurepip
    # In macos versions El Capitan and later, you must deal with System Integrity Protection
    pip2 install --user pygithub3

Run like this:

bash get-recent-repos.sh <gilab-read-only-access-token> 
#!/bin/bash
set -x
rm -rf data
mkdir data
cp list-all-repos.py data
cd data
touch all-repos.txt recent-repos.txt
for url in $(python2.7 list-all-repos.py ConsenSys | tr -d '"'); do echo $url >> repos.txt; done
for n in {1..30} 
do 
    for repo in $(curl --header "PRIVATE-TOKEN:$1" -s "https://gitlab.com/api/v4/groups/ConsenSys/projects?include_subgroups=true&per_page=100&page=$n" | jq '.[].ssh_url_to_repo' | tr -d '"'); do echo $repo >> all-repos.txt; done;
done

sort all-repos.txt -o all-repos.txt 
while read line; do
    git clone $line --depth 1    
    reversed=$(echo $line | sed $'s/./&\\\n/g' | sed -ne $'x;H;${x;s/\\n//g;p;}')
    stripped=$(echo $reversed | sed 's/tig.//g' | sed 's/\/.*$//g' | tr -d '/' )
    directory=$(echo $stripped | sed $'s/./&\\\n/g' | sed -ne $'x;H;${x;s/\\n//g;p;}')
    cd $directory
        year=$(git show -s --date=format:'%Y' --format=%cd)
        cd -
        if [[ $year="2019" || $year="2020" || $year="2021"]]; then
           echo $line >> recent-repos.txt
        fi
    rm -rf $directory
done < all-repos.txt
cd ..
cp data/recent-repos.txt .
rm -rf data